---
marp: true
title: "How to use Nix"
author: haruki7049
paginate: true
theme: default
class: invert
footer: "https://gitlab.com/haruki7049/how-to-use-nix"
---

# How to use Nix

- author: haruki7049
- twitter: https://twitter.com/Love__Yoshi

---

1. What is Nix and NixOS?
2. How to install Nix
3. Grammar of Nix-lang

---

# What is Nix and NixOS?

Nixは再現可能な開発環境を作成する事が出来ます。Nixを使用する時には、独自に作成されたNix言語を使用する必要があります。NixOSも同様に、Nix言語を使用して再現可能なPC環境を作成する事が可能になります。

```nix
with import <nixpkgs> {}:
stdenv.mkDerivation {
  pname = "hoge";
  version = "0.1";

  src = fetchGitHub {
    ...
  }
}
```

---

# How to install Nix

- Default: `sh <(curl -L https://nixos.org/nix/install) --daemon`
- archlinux: `pacman -S nix`

---

# Grammar of Nix-lang

とりあえずこれ読んで
https://nixos.org/manual/nix/stable/language/index.html
